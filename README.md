<div align="center">
<h1 align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/18761392/cantalogo.jpg" alt="Project">
  <br />
  Canta Themes for Snaps
</h1>
</div>

<p align="center"><b>These are the Canta GTK and icon themes for snaps</b>. Canta is a flat Material Design theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Pantheon, XFCE, Mate, etc.


This snap contains all flavours of the GTK and icon themes.

<b>Attributions</b>:
This snap is packaged from vinceliuice's <a href="https://github.com/vinceliuice/Canta-theme">Canta-theme</a>. The Canta theme is under a GPL3.0 licence.

</p>


## Install 
<a href="https://snapcraft.io/canta-themes">
<img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

You can install the theme from the Snap Store оr by running:

```
sudo snap install canta-themes
```
To connect the theme to an app run:
```
sudo snap connect [other snap]:gtk-3-themes canta-themes:gtk-3-themes 
```
```
sudo snap connect [other snap]:icon-themes canta-themes:icon-themes
```


